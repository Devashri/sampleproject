/*
  Change: 
  Sketch title
  
  Describe what it does in layman's terms.  Refer to the components
  attached to the various pins.

  The circuit:
  * list the components attached to each input
  * list the components attached to each output

  https://www.arduino.cc/en/Guide/HomePage

*/

#include<MuxShield.h>
#include<math.h>

#define PRESSURE_TOLERANCE 0.04//PRESSURE_TOLERANCE is the control tolerance (smaller -> large settling time, larger -> bad tolerance)
#define MAX_PRESSURE_IN_PSI 3.0

#define NUMBER_OF_BUBBLES 25
#define INIT_PRESSURE 1
#define PRESSURE_VALUE 104
#define BAUD_RATE 250000
#define CUTOFF_FREQUENCY 00                 // Low pass filter cutoff frequency

#define ASCII_INPUT 'a'
#define BINARY_INPUT 'b'
#define SET_PRESSURE_MAP 'd'
#define EXHAUST 'e'
#define BOTTOM_UP_APPROACH 'f'
#define TOP_DOWN_APPROACH 'g'

#define CHAR_Z 'z'

/* Initializes muxshield II */
MuxShield muxShield;

/*
   time_now, time_prev are used to calculate arduino transmission frequency
   exhaust_time determines how long all the bubbles are exhausted
*/
unsigned long time_now, time_prev, exhaust_time = 5000;

/*
   freq, INITIAL_FREQUENCY are used to calculate arduino transmission frequency
*/
float freq, INITIAL_FREQUENCY = 1;

/*
   ascii_switch is a display flag
   ascending represents either top down or bottom up approach
*/
boolean ascii_switch = false, diag_flag = false, done = false, ascending = true;

/* Array of all the digital pin numbers connected to valves */
uint8_t digital_pins[] = {22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 20, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39};

boolean offloading_flags[25] = {false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false};

int counter=0;
byte mask =1;
/*
   pwm_val  = pwm_const * (Pressure in psi) ; pwm_val is a 8 bit value
   pwm_pin is the digital pin connected to proportional controller
*/
uint8_t pwm_const, pwm_pin = 9, pwm_val = 0;

/*
   Pressures - array of sensed pressures in bubbles
   setPressures - aray of commanded pressures in bubbles (Initially, commanded pressures for all the bubbles are set to 1)
*/
float Pressures[NUMBER_OF_BUBBLES], setPressures[NUMBER_OF_BUBBLES];

/* 10 bit counterparts of Pressures, setPressures*/
uint16_t pressures[NUMBER_OF_BUBBLES], setpressure[NUMBER_OF_BUBBLES];

/* An array of sorted indices */
uint8_t sorted_indices[NUMBER_OF_BUBBLES] ;
int high_pins = 0;

/* An array for storing arduino serial read buffer */
/* why [64] ?*/
byte readbuffer[64];
byte * bpointer = (byte *) &freq;                   // bpointer points to the location of freq

/* The code inside the setup() is run only once.
   Initialization code is present in setup()
*/
void setup() {

  loadPressureValues();
  
  muxShield.setMode(1, ANALOG_IN);                  // Set bank 1 of Muxshield to analog input
  muxShield.setMode(2, ANALOG_IN);                  // Set bank 2 of Muxshield to analog input

  Serial.begin(BAUD_RATE);                          // Set baud rate of the serial port to 250000
  Serial.flush();                                   // Flushes the serial write buffer on the arduino
  pinMode(pwm_pin, OUTPUT);                         // Set the pwm_pin (pin number 9) connected to proportional air regulator to digital out

  for (int i = 0; i < NUMBER_OF_BUBBLES; i++)       // The code inside the loop is run for all the 25 pins
  {
    pinMode(digital_pins[i], OUTPUT);               // Set each digital pin connected to valves to output
    sorted_indices[i] = i;                          // Initialize the indices array
    setPressures[i] = 2 * setPressures[i];          // modify the initialized setPressure array
  }
  setPressures[0] = 1;                              // modify the initialized setPressure array
  pwm_const = 21;                                   // modify the pwm_const (trial and error, tuning)
}

/*
   The code in loop() runs repeatedly.
*/
void loop() {

  int num_of_bytes = Serial.available();                                                                              // Read the number of bytes available at the arduino read buffer
  
  if (micros() != 0)                                                                                             
    freq = (1000000.0 / (micros() - time_prev) + (CUTOFF_FREQUENCY) * INITIAL_FREQUENCY) / (1 + CUTOFF_FREQUENCY);    // Low pass filter for frequency calculation
  time_now = micros();                                                                                                // record the relative time

  //readPressureData();
  
  if (num_of_bytes)
    Serial.readBytesUntil(char(10), readbuffer, 64);                                                                  // Read bytes until newline character and transfer to readbuffer[]

  /*
   * Newline character is encountered. Transfer the bytes to the readbuffer.
   * readbuffer[1] == 0??
   * number of bytes == 2??
   */
  if (readbuffer[1] == 0 && num_of_bytes == 2)
  {
    switch ((char)readbuffer[0]) {
      case ASCII_INPUT:                                 // toggles ascii display on
        ascii_switch = true;
        break;
      case BINARY_INPUT:                                // toggles ascii display off / Change: Sets binary display on
        ascii_switch = false;                           
        break;
      case EXHAUST:                                     // Exhaust all volumes for exhaust_time(milliseconds)
        exhaust();
        break;
      case BOTTOM_UP_APPROACH:                          // Set the given pressure map simultaneously (bottom up)
        ascending = true;
        pressure_sorted_regulate();
        break;
      case TOP_DOWN_APPROACH:                           // Set the given pressure map simultaneously (top down)
        ascending = false;
        pressure_sorted_regulate();
        break;
    }
  }

  /*
   * condition is : number of bytes present is 51 --> num_of_bytes == 51 and readbuffer[52] == 0 ??
  */
  if (num_of_bytes == 55 && readbuffer[56] == 0)                          // Read and store 10 bit commanded pressure map
  {
    for (int i = 0; i < NUMBER_OF_BUBBLES; i++)
    {
      setpressure[i] = (uint16_t) readbuffer[2 * i + 1] << 8;             // Rearranging the byte values between the LSB and MSB
      setpressure[i] |= (uint16_t) readbuffer[2 * i];                     // Change: 0.04 and 16.11??
      setPressures[i] = (((setpressure[i] / 1023.0) - 0.04) * 16.11);     // Convert 10 bit pressure value into human readable psi values
    }

    int bytenum = 50;
    mask = 1;
    if ((readbuffer[bytenum] & mask)) 
    {mask = 2;  
    for (int counter=0; counter<25;counter++) 
    { //iterate through bit mask
    if (!(readbuffer[bytenum] & mask)) // if bitwise AND resolves to true
      offloading_flags[counter]=true;   
    else
      offloading_flags[counter]=false;
    mask <<= 1;
    if (mask==0)  
      {bytenum++;
      mask=1;}
    }}
  }

  if (ascii_switch)
    performASCII_InputHandling();
  else
    performBinary_InputHandling();

  time_prev = time_now;
  INITIAL_FREQUENCY = freq;
  memset(readbuffer, 0, sizeof(readbuffer));                   //Clearing the readbuffer using memset 

}

/*
 * Load the pressure values to the arrays setPressures and setpressure
 */
void loadPressureValues(){
  for (int i = 0; i < NUMBER_OF_BUBBLES; i++){
    setPressures[i] = INIT_PRESSURE;
    setpressure[i] = PRESSURE_VALUE;
  }
}

/*
 * Read pressure sensor data
 */
void readPressureData(){
  for (int i = 0; i < NUMBER_OF_BUBBLES; i++)
  {
    if (i < 16)
      pressures[i] = muxShield.analogReadMS(2, i);
    else
      pressures[i] = muxShield.analogReadMS(1, i - 16);
  }
}

/*
void performASCII_InputHandling(){
    generateASCII_InputPattern();
    analogWrite(pwm_pin, 0);                                
}
*/

/*
 * In the serial monitor, print comma separated values of pressures(in ASCII) in psi followed by arduino frequency
 */
void performASCII_InputHandling(){
    readPressureData();
    Serial.print(ASCII_INPUT);                                            // The first character is 'a'
    for (int i = 0; i < NUMBER_OF_BUBBLES; i++)
    {
      Serial.print(((pressures[i] / 1023.0) - 0.04) * 16.11, 1);          // Value inside the print statement ??
      Serial.print(',');                                                  // Delimiter is comma between the pressure values
    }
    Serial.print((int)freq);                                              // Frequency is sent after all the pressure values
    Serial.println(CHAR_Z);                                               // The last character is 'z'
}

/*
 * In the serial monitor, print comma separated values of pressures(in bytes) in psi followed by arduino frequency
 */
void performBinary_InputHandling(){
    readPressureData();
    Serial.print(BINARY_INPUT);                                           // The first character is 'b'
    Serial.write((byte*)pressures, 50);                                   // The 25 pressure values are sent as 2-byte values
    Serial.write(bpointer, sizeof(float));                                // The frequency is a float value
    Serial.println(CHAR_Z);                                               // The last character is 'z'
}

//void pressure_sequential_regulate()
//{
// for (int i=0; i<25; i++)
//  {
//    //Analog read on all 16 inputs on IO1, IO2, and IO3
//    if (i<16){
//    Pressures[i] = (((muxShield.analogReadMS(2,i) / 1023.0) -0.04)*16.11);}
//    else {
//    Pressures[i] = (((muxShield.analogReadMS(1,i-16) / 1023.0) -0.04)*16.11);}
//
//    analogWrite(pwm_pin, uint8_t(19.52*setPressures[i]-2.8));
//    pwm_val = uint8_t(pwm_const*setPressures[j[i]]);
//    digitalWrite(selectPin[i], HIGH);
//    while (abs(Pressures[i]-setPressures[i]) > pTol)
//    {if (i<16){
//    Pressures[i] = (((muxShield.analogReadMS(2,i) / 1023.0) -0.04)*16.11);}
//    else {
//    Pressures[i] = (((muxShield.analogReadMS(1,i-16) / 1023.0) -0.04)*16.11);}
//    //Serial.println(Pressures[i],1);
//      }
//    digitalWrite(selectPin[i], LOW);
//
//  }
//}

/*
 * main purpose of this function??
 */
void pressure_sorted_regulate()
{
  if (ascending)
    sort_indices_bottom_up(setPressures, NUMBER_OF_BUBBLES, sorted_indices);
  else
    sort_indices_top_down(setPressures, NUMBER_OF_BUBBLES, sorted_indices);
  /*
   * The switch of the each bubble is turned ON.
   * If any bubble is of more than 3.0 psi, it is loaded with 3.0 psi
   */
  high_pins = 0;

  for (int i = 0; i < NUMBER_OF_BUBBLES; i++)
  {
    if (!offloading_flags[i])
    {digitalWrite(digital_pins[i], HIGH);
    high_pins++;
    if (setPressures[i] > MAX_PRESSURE_IN_PSI)
      setPressures[i] = MAX_PRESSURE_IN_PSI;
    }
    else 
    digitalWrite(digital_pins[i], LOW);
  } 
  
  for (int i = 0; i < NUMBER_OF_BUBBLES; i++)
  {
    if (!offloading_flags[sorted_indices[i]])
    {
    if (sorted_indices[i] < 16)                                                                                             //Analog read on all 16 inputs on IO1, IO2, and IO3
      Pressures[sorted_indices[i]] = ((((muxShield.analogReadMS(2, sorted_indices[i]) - (int)(0.0*(high_pins))) / 1023.0) - 0.04) * 16.11) ;
    else
      Pressures[sorted_indices[i]] = ((((muxShield.analogReadMS(1, sorted_indices[i] - 16) - (int)(0.0*(high_pins))) / 1023.0) - 0.04) * 16.11);

    //if (high_pins< 4)
    //  high_pins = high_pins/1.5;
    //pwm_val = uint8_t(19.52*setPressures[sorted_indices[i]]-2.8);
    pwm_val = uint8_t(pwm_const * (setPressures[sorted_indices[i]]+ 0.008*(high_pins) + 0.0));
    analogWrite(pwm_pin, pwm_val);

    while ((abs(Pressures[sorted_indices[i]] - 0.008*(high_pins) - setPressures[sorted_indices[i]]) > PRESSURE_TOLERANCE) )
    {
        for (int j = 0; j < NUMBER_OF_BUBBLES; j++)
        {
        if (j < 16)
          pressures[j] = muxShield.analogReadMS(2, j) - (int)(0.0*(high_pins));
        else
          pressures[j] = muxShield.analogReadMS(1, j - 16) - (int)(0.0*(high_pins));
        }
      if (sorted_indices[i] < 16)                                                                                             //Analog read on all 16 inputs on IO1, IO2, and IO3
        Pressures[sorted_indices[i]] = ((((muxShield.analogReadMS(2, sorted_indices[i]) ) / 1023.0) - 0.04) * 16.11) ;
      else
        Pressures[sorted_indices[i]] = ((((muxShield.analogReadMS(1, sorted_indices[i] - 16) ) / 1023.0) - 0.04) * 16.11);

        Pressures[sorted_indices[i]] = ((((pressures[sorted_indices[i]] ) / 1023.0) - 0.04) * 16.11) - 0.0;
/*
 
      for (int i = 0; i < NUMBER_OF_BUBBLES; i++)                        
      {
        if (i < 16)
          pressures[i] = muxShield.analogReadMS(2, i);
        else
          pressures[i] = muxShield.analogReadMS(1, i - 16);
      }
*/
      if (ascii_switch)
        performASCII_InputHandling();
      else
        performBinary_InputHandling();
        Serial.print(BINARY_INPUT);                                           // The first character is 'b'
        Serial.write((byte*)pressures, 50);                                   // The 25 pressure values are sent as 2-byte values
        Serial.write(bpointer, sizeof(float));                                // The frequency is a float value
        Serial.println(CHAR_Z);                                               // The last character is 'z'
    }
    digitalWrite(digital_pins[sorted_indices[i]], LOW);
    high_pins--;
  }
  }
}

/*
 * purpose of this function??
 */
void exhaust()
{
  unsigned long temp_time = millis();
  
  analogWrite(pwm_pin, 0);
  turnPINStoHigh();
//    for (int i = 0; i < 15; i++)
//    digitalWrite(digital_pins[i], HIGH);
  
  while (millis() - temp_time < exhaust_time)
  {
    
    for (int i = 0; i < NUMBER_OF_BUBBLES; i++)
        {
        if (i < 16)
          pressures[i] = muxShield.analogReadMS(2, i) - 13;
        else
          pressures[i] = muxShield.analogReadMS(1, i - 16) -13;
        }
        
    if (ascii_switch)
      performASCII_InputHandling();
    else
    {      
      Serial.print(BINARY_INPUT);                                           // The first character is 'b'
      Serial.write((byte*)pressures, 50);                                   // The 25 pressure values are sent as 2-byte values
      Serial.write(bpointer, sizeof(float));                                // The frequency is a float value
      Serial.println(CHAR_Z);                                               // The last character is 'z'
    }
  }
  turnPINStoLow();
}

/*
 * Change: Rename a[] to sensedPressure[] or commanded_pressure[]
 *         Rename size to number_of_bubbles or the const variable
 * Sorting the indices in the bottom up approach
 */
void sort_indices_bottom_up(float a[], uint8_t size, uint8_t indices[]) {
  for (int i = 0; i < (size - 1); i++) {
    for (int o = 0; o < (size - (i + 1)); o++) {
      if (a[indices[o]] > a[indices[o + 1]]) {
        uint8_t t = indices[o];
        indices[o] = indices[o + 1];
        indices[o + 1] = t;
      }
    }
  }
}

/*
 * Change: Rename a[] to sensedPressure[] or commanded_pressure[]
 *         Rename size to number_of_bubbles or the const variable
 * Sorting the indices in the top down approach
 */
void sort_indices_top_down(float a[], uint8_t size, uint8_t indices[]) {
  for (int i = 0; i < (size - 1); i++) {
    for (int o = 0; o < (size - (i + 1)); o++) {
      if (a[indices[o]] < a[indices[o + 1]]) {
        uint8_t t = indices[o];
        indices[o] = indices[o + 1];
        indices[o + 1] = t;
      }
    }
  }
}

/*
 * Turn on the pins which are corresponding to all the bubbles
 */
void turnPINStoHigh(){
  for (int i = 0; i < NUMBER_OF_BUBBLES; i++)
    digitalWrite(digital_pins[i], HIGH);
}

/*
 * Turn off the pins which are corresponding to all the bubbles
 */
void turnPINStoLow(){
  for (int i = 0; i < NUMBER_OF_BUBBLES; i++)
    digitalWrite(digital_pins[i], LOW);
}
